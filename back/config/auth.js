var passport = require('passport'), LocalStrategy = require('passport-local').Strategy, BearerStrategy = require('passport-http-bearer').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var sanitizer = require('sanitizer');
var jwt = require('jsonwebtoken')


module.exports = passport;